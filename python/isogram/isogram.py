def is_isogram(string):
    string = ''.join(string.split())
    string = string.lower()

    for c in string:
        if c.isalpha():
            if string.count(c) > 1:
                return False

    return True
