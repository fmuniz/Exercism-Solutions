class Matrix:
    def __init__(self, matrix_string):
        self.string = matrix_string
        self.matrix = []
        for line in self.string.split('\n'):
            row = []
            for item in line.split():
                row.append(int(item))
            self.matrix.append(row)

        self.rows = len(self.matrix)
        self.columns = len (self.matrix[0])

    def row(self, index):
        self.index = index - 1
        return self.matrix[self.index]

    def column(self, index):
        self.index = index - 1
        columnx = []
        for r in range(self.rows):
        	columnx.append(self.matrix[r][self.index])
        return columnx
